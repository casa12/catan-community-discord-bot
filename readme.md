# Basic workflow

Start a game with `!creategame`, wait for people to join.

When you start, type `!start` so lock the users for the game.

When the game is over, type `!winner` to declare a winner.

# Common Bot Commands

Commands tagged with ✈️ can be sent over direct message.

Commands tagged with 🛡️ can only be used by moderators

### `!creategame [wager]`

Make a game. React to the result to join a game. Joining a game will add 25 lp to the prize pool. Wager is optional.

You can optionally write a link after this command which will be copied to the response. Examples:

Create a game with 0 wager:

    !creategame

Create a game with 0 wager and a link:

    !creategame https://colonist.io/#mouse

Create a game with 25 wager: (winner of a 4 player game with 25 wager will earn 175 points)

    !creategame 25

Make a game with both a wager and a link:

    !creategame 25 https://colonist.io/#mouse

Tag colonist in the same message:

    !creategame 25 https://colonist.io/#mouse @colonist 25 wager game join now!

### `!winner [game id] [@winner]`

Finish a game. This message must include a screenshot of the final scores. All players must react to the message to
confirm.

Example:

     !winner SilverWheat242F @mousetail

### `!rank` ✈️

Shows your rank and total lp.

### `!rank [@user]`

get a specific users rank

### `!lphistory` ✈️

get your most recent LP events

### `!lphistory [@user]`

get a users LP history

###`!netwager` ✈️ 

get your net amount of wagered LPs

### `!netwager [@user]`

get a users net amount of wagered LPs

### `!leaderboard [time period]` ✈️

Shows the 25 best players. Period can be either `month`, `season` or `all`. Default
is month. The win rate is also calculated based on the month.

### `!help` ✈️

️Shows this page.

### `!start [game id]`

Mark a game as started, so that no further players can join.

For example:

    !start SilverWheat242F

### `!remove [game id] [user mention]`

Remove a user from a game. Please ask a player to leave first, only kick them
if they won't respond.

For example:

    !remove SilverWheat298K @mousetail

# Fun Commands

### `!quote`

Learn what history can teach us about how to play catan.

If you have ideas for more quotes you can edit the
file [here](https://gitlab.com/mousetail/catan-community-discord-bot/-/blob/master/data/quotes.txt). All changes need
to be approved.

# Mod Commands

These commands require the user to be admin/mod.

### `!adminwinner [game id] [@winner]` 🛡️

Immediately closes a match and awards the specified winner VP.

Example:

    !adminwinner SilverWhat242F @Puzzles

### `!adminlp [@user] lp [games]` 🛡️

Gives \@user lp according to `lp`. You can use `!adminlp [@user] 0` to add a user to the leaderboard without giving them
any lp.

Example of giving Hippo 25 LP:

    !adminlp @Hippo 25

Add NordicZombie to the leaderboard:

    !adminlp @NordicZombie 0

Add one game and 100 LP to ReMarkable:

    !adminlp @ReMarkable 100 1

Subtract one LP from Mousetail

    !adminlp @mousetail -1

### `!admincancel [game id]` 🛡️

Cancels a game. If the game is finished refund lp. If not, set a flag that prevents if from being `!winner` from
working.

Example:

    !admincancel SilverWheat242F

### `!adminlplist` 🛡️✈️

Gets all players LP's as a CSV file. Guaranteed to work upto 500 players.

### `!lpupload` 🛡️✈️

Adds LP to all players based on a CSV file. File should be ; seperated and have 3 columns:
Discord name, (ignored), LP

### `!adminsetting` 🛡️✈️

Sets game settings, options are:

| Setting | Meaning | 
| :----   | :------ |
| link    | The link spammed in `!rank` and `!leaderboard` commands. |
| min_wager | The minimal wager amount allowed, 0 wagers are always allowed |
| max_wager | The maximum wager allowed per person per match |
| wager_step | The wager must be a multiple of `wager_step`. Can be set to 1 to disable the step. |
| report_channel | The ID for the channel reports should be sent to. |

To change the maximum wager to 100:

     !adminsetting max_wager 100

To set the link to this page:

     !adminsetting link https://catan-community-website-2.vercel.app/info

To disable wagers all together:

    !adminsetting max_wager 0
    !adminsetting min_wager 100

# Mod Management Commands

### `!adminrole [@role]` 🛡️✈️

Adds `role` to the list of roles permitted to use admin commands.

For example:

    !adminrole @tournamentSupport

### `!unadminrole [@role]` 🛡️✈️

Removes `role` form the list of roles permitted to use admin commands. The server owner can always use admin commands
regardless of the role settings.

# Contributing

I am just one guy and also have a full time job, so won't always be able to fix all the issues with the bot. If you find
a problem and don't want to wait for me to fix it, just do it yourself! The code is
right [here](https://gitlab.com/mousetail/catan-community-discord-bot), just submit a pull request, and if it's a good
suggestion I'll be sure to merge it in.

Current contributors:

* SupHip
* TBYNL
* mousetail
* you?
