import asyncio
import db as database


async def generate_data(db):
    users = db["users"].find()
    async for user in users:
        print("processing " + user.get("display_name", user["discord_username"]))
        matches = db["matches"].find(
            {
                "players": user["discord_username"]
            }
        )
        nrof_wins = 0
        nrof_matches = 0
        async for match in matches:
            if match["winner"] == user["discord_username"]:
                nrof_wins += len(match["players"])
            nrof_matches += 1

        await db["users"].update_one(
            {
                "_id": user["_id"]
            },
            {
                "$set": {
                    "matches": nrof_matches,
                    "normalized_wins": nrof_wins
                }
            }
        )


if __name__ == "__main__":
    db = database.get_db(False)

    asyncio.run(
        generate_data(db)
    )
